angular.module('sampleApp', ['ngMaterial', 'ui.router', 'ngResource'])
    .config(function($stateProvider, $urlRouterProvider) {

    'use strict';

    $stateProvider

    // home page
    .state('app', {
        url: '/',
        views: {
            'header': {
                templateUrl : 'views/header.html'
            },
            'content': {
                templateUrl: 'views/home.html',
                controller: 'homeController'
            },
            'footer': {
                templateUrl : 'views/footer.html'
            }
        }
    })

    // Nerds
    .state('app.nerds', {
        url: 'nerds',
        views: {
            'content@': {
                templateUrl: 'views/nerd.html',
                controller: 'nerdController'
            }
        }
    });

    $urlRouterProvider.otherwise('home');
});

var app = angular.module('sampleApp');

app.controller('nerdController', [ '$scope', '$stateParams', 'nerdFactory', function($scope, $stateParams, nerdFactory) {
        $scope.nerd = {};
        $scope.showNerd = false;
        $scope.message="Loading...";


        nerdFactory.getNerds().query(
        function(response) {
            $scope.nerds = response;
            $scope.showNerd = true;
        },
        function(response) {
            $scope.message = "Error: "+response.status + " " + response.statusText;
        });

}]);

var app = angular.module('sampleApp');

app.controller('homeController', [ '$scope', '$stateParams', 'homeService', 'homeServiceSection2', function($scope, $stateParams, homeService, homeServiceSection2) {
        $scope.homeContent = {};
        $scope.homeSectionContent = {};
        $scope.showHome = false;
        $scope.message="Loading...";


        homeService.getHomeContent().query(
        function(response) {
            $scope.homeContent = response;
            $scope.showHome = true;
        },
        function(response) {
            $scope.message = "Error: "+response.status + " " + response.statusText;
        });

        homeServiceSection2.getHomeSection_2_Content().query(
            function(response){
               $scope.homeSectionContent = response; 
            },
            function(response){
                $scope.message = "Error: "+response.status + " " + response.statusText;
            });

}]);

angular.module('sampleApp')
    .constant("baseURL", "http://localhost:3000/")

    // Nerd Factory (Sample)
    .service('nerdFactory', ['$resource','baseURL', function($resource, baseURL) {
          this.getNerds = function(){
              return $resource(baseURL+"nerds/:id", null,{'update':{method:'GET'}});
          };
}]);

angular.module('sampleApp')
    .constant("baseURL", "http://localhost:3000/")

    // Home sevice (Sample)
    .service('homeService', ['$resource','baseURL', function($resource, baseURL) {
          this.getHomeContent = function(){
              return $resource(baseURL+"home/:id", {section: 'index'},{'update':{method:'GET'}});
          };
	}])
    .service('homeServiceSection2', ['$resource','baseURL', function($resource, baseURL) {
          this.getHomeSection_2_Content = function(){
              return $resource(baseURL+"home/:id", {section: 'section-2'},{'update':{method:'GET'}});
          };
}]);
