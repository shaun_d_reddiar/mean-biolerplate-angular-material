var app = angular.module('sampleApp');

app.controller('homeController', [ '$scope', '$stateParams', 'homeService', 'homeServiceSection2', function($scope, $stateParams, homeService, homeServiceSection2) {
        $scope.homeContent = {};
        $scope.homeSectionContent = {};
        $scope.showHome = false;
        $scope.message="Loading...";


        homeService.getHomeContent().query(
        function(response) {
            $scope.homeContent = response;
            $scope.showHome = true;
        },
        function(response) {
            $scope.message = "Error: "+response.status + " " + response.statusText;
        });

        homeServiceSection2.getHomeSection_2_Content().query(
            function(response){
               $scope.homeSectionContent = response; 
            },
            function(response){
                $scope.message = "Error: "+response.status + " " + response.statusText;
            });

}]);
