var app = angular.module('sampleApp');

app.controller('nerdController', [ '$scope', '$stateParams', 'nerdFactory', function($scope, $stateParams, nerdFactory) {
        $scope.nerd = {};
        $scope.showNerd = false;
        $scope.message="Loading...";


        nerdFactory.getNerds().query(
        function(response) {
            $scope.nerds = response;
            $scope.showNerd = true;
        },
        function(response) {
            $scope.message = "Error: "+response.status + " " + response.statusText;
        });

}]);
