angular.module('sampleApp', ['ngMaterial', 'ui.router', 'ngResource'])
    .config(function($stateProvider, $urlRouterProvider) {

    'use strict';

    $stateProvider

    // home page
    .state('app', {
        url: '/',
        views: {
            'header': {
                templateUrl : 'views/header.html'
            },
            'content': {
                templateUrl: 'views/home.html',
                controller: 'homeController'
            },
            'footer': {
                templateUrl : 'views/footer.html'
            }
        }
    })

    // Nerds
    .state('app.nerds', {
        url: 'nerds',
        views: {
            'content@': {
                templateUrl: 'views/nerd.html',
                controller: 'nerdController'
            }
        }
    });

    $urlRouterProvider.otherwise('home');
});
