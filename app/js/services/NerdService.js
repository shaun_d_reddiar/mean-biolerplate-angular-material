angular.module('sampleApp')
    .constant("baseURL", "http://localhost:3000/")

    // Nerd Factory (Sample)
    .service('nerdFactory', ['$resource','baseURL', function($resource, baseURL) {
          this.getNerds = function(){
              return $resource(baseURL+"nerds/:id", null,{'update':{method:'GET'}});
          };
}]);
