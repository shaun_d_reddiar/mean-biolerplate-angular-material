angular.module('sampleApp')
    .constant("baseURL", "http://localhost:3000/")

    // Home sevice (Sample)
    .service('homeService', ['$resource','baseURL', function($resource, baseURL) {
          this.getHomeContent = function(){
              return $resource(baseURL+"home/:id", {section: 'index'},{'update':{method:'GET'}});
          };
	}])
    .service('homeServiceSection2', ['$resource','baseURL', function($resource, baseURL) {
          this.getHomeSection_2_Content = function(){
              return $resource(baseURL+"home/:id", {section: 'section-2'},{'update':{method:'GET'}});
          };
}]);
