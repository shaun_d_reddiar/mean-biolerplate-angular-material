var gulp = require('gulp'),
	sass = require('gulp-sass'),
	browserSync  = require('browser-sync'),
	usemin = require('gulp-usemin'),
	del = require('del');
	uglify = require('gulp-uglify'),
	jshint = require('gulp-jshint'),
	stylish = require('jshint-stylish'),
	rev = require('gulp-rev'),
	notify = require('gulp-notify'),
	ngannotate = require('gulp-ng-annotate'),
	minifycss = require('gulp-minify-css'),

	watchFiles = [
		'./app/css/**/*.css',
		'./app/js/**/*.js',
		'./app/**/*.html'
	],
	watchTasks = [
					'clean',
					'sass',
					'usemin',
					'copyfiles',
					'copy_lib_files_to_public'
				 ];

// LSHint and Clean Task
gulp.task('jshint', function() {
return gulp.src('app/js/**/*.js')
.pipe(jshint())
.pipe(jshint.reporter(stylish));
});

// Clean public folder
gulp.task('clean', function() {
    return del(['public']);
});

// Usemin task
gulp.task('usemin',['jshint'], function () {
  return gulp.src('./app/index.html')
      .pipe(usemin({
      	assetsDir: './app',
        css:[minifycss(),rev()],
        js: []
        // js: [ngannotate(), uglify(),rev()]
      }))
      .pipe(gulp.dest('public/'));
});

//copy html files
gulp.task('copyhtml',['usemin'], function(){
   gulp.src('./app/views/**')
   .pipe(gulp.dest('./public/views/'));
});

gulp.task('copyfiles', function() {

   // Copy libraries to lib folder
   // angular
   gulp.src('./bower_components/angular/angular.min.js')
   .pipe(gulp.dest('./app/libs'));

   // angular-animate
   gulp.src('./bower_components/angular-animate/angular-animate.min.js')
   .pipe(gulp.dest('./app/libs'));

   // angular-aria
   gulp.src('./bower_components/angular-aria/angular-aria.min.js')
   .pipe(gulp.dest('./app/libs'));

   // angular-messages
   gulp.src('./bower_components/angular-messages/angular-messages.min.js')
   .pipe(gulp.dest('./app/libs'));

   // angular-material
   // js
   gulp.src('./bower_components/angular-material/angular-material.min.js')
   .pipe(gulp.dest('./app/libs'));
   // css
   gulp.src('./bower_components/angular-material/angular-material.min.css')
   .pipe(gulp.dest('./app/libs'));

   // angular-resource
   gulp.src('./bower_components/angular-resource/angular-resource.min.js')
   .pipe(gulp.dest('./app/libs'));

   // angular-ui-router
   gulp.src('./bower_components/angular-ui-router/release/angular-ui-router.min.js')
   .pipe(gulp.dest('./app/libs'));
});

gulp.task('copy_lib_files_to_public',['clean', 'copyfiles'], function(){
	// Copy lib folder into public lib folder
	gulp.src('./app/libs/**')
	.pipe(gulp.dest('./public/libs'));

	// Copy icons used
	gulp.src('./app/img/icons/**')
	.pipe(gulp.dest('./public/img/icons'));
});



// Compile css
gulp.task('sass', function(){
	return gulp.src(['./app/sass/**/**/*.scss', './app/sass/**/*.scss'])
	.pipe(sass.sync().on('error', sass.logError))
	.pipe(gulp.dest('./app/css'));
});


// Browser sync
gulp.task('browser-sync', ['default'], function () {
	browserSync.init(watchFiles, {
	      server: {
	         baseDir: "public",
	         index: "index.html",
	         proxy: "http://localhost:3000/"
	      }
	});
});


gulp.task('watch', ['browser-sync'], function(){
	// Watch html index file
	gulp.watch('app/index.html', ['usemin']);
	// Watch html view files
	gulp.watch('app/views/**/*.html', ['copyhtml']);
	// Watch js files
	gulp.watch('app/js/**/*.js', ['usemin']);
	// Watch css files
	gulp.watch('app/css/**/**/*.css', ['usemin']);
	
	// reload the browser if any change happens in the public folder
	//gulp.watch('public/**').on('change', browserSync.reload);

	// Show a notice on every update
	gulp.watch('public/**').on('change', function(){
		gulp.src('public').pipe(notify({ message: 'Update completed!' }));
	});
});

gulp.task('default', ['clean', 'sass','usemin','copyhtml', 'copyfiles', 'copy_lib_files_to_public'], function(){
		gulp.src('public').pipe(notify({ message: 'Project built!' }));
});

